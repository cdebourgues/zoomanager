package View.Personnel;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Andre on 10.03.2017.
 */
public class PersonnelInfo extends JPanel{

    public PersonnelInfo() {
        // Sous ensemble de gauche contenant les détails sur les employées
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        // Liste déroulante avec les noms et prénom des employé
        JComboBox boxChoicePersonne = new JComboBox();
        JPanel choicePersonnel = new JPanel();
        // Requête pour récupérer les nom et prénoms des employé
        boxChoicePersonne.addItem("Guillaume Tell");
        boxChoicePersonne.addItem("Arsène Lupin");
        choicePersonnel.add(boxChoicePersonne);
        this.add(choicePersonnel);


        // Requête pour récuperer les détails d'une personne
        // Nom
        JPanel detailFirstName = new JPanel();
        JLabel firstNameLabel = new JLabel("Nom");
        JLabel firstName = new JLabel("Marley");
        detailFirstName.add(firstNameLabel);
        detailFirstName.add(Box.createHorizontalStrut(20));
        detailFirstName.add(firstName);
        this.add(detailFirstName);

        // Prénom
        JPanel detailLastName = new JPanel();
        JLabel lastNameLabel = new JLabel("Prénom");
        JLabel lastName = new JLabel("Bob");
        detailLastName.add(lastNameLabel);
        detailLastName.add(Box.createHorizontalStrut(20));
        detailLastName.add(lastName);
        this.add(detailLastName);

        // Age
        JPanel detailBirthday = new JPanel();
        JLabel birthdayLabel = new JLabel("Date de Naissance");
        JLabel birthday = new JLabel("09/02/1983");
        detailBirthday.add(birthdayLabel);
        detailBirthday.add(Box.createHorizontalStrut(20));
        detailBirthday.add(birthday);
        this.add(detailBirthday);

        // Arrivé
        JPanel detailBirthdayIncorporation = new JPanel();
        JLabel birthdayIncorporationLabel = new JLabel("Date d'arrivé");
        JLabel birthdayIncorporation = new JLabel("01/03/2017");
        detailBirthdayIncorporation.add(birthdayIncorporationLabel);
        detailBirthdayIncorporation.add(Box.createHorizontalStrut(20));
        detailBirthdayIncorporation.add(birthdayIncorporation);
        this.add(detailBirthdayIncorporation);

        // Adresse
        JPanel detailAddress = new JPanel();
        JLabel addressLabel = new JLabel("Adresse");
        JLabel address = new JLabel("Avenue de Tourbillon 21");
        detailAddress.add(addressLabel);
        detailAddress.add(Box.createHorizontalStrut(20));
        detailAddress.add(address);
        this.add(detailAddress);

        // Ville
        JPanel detailCity = new JPanel();
        JLabel cityLabel = new JLabel("Ville");
        JLabel city = new JLabel("Sion");
        detailCity.add(cityLabel);
        detailCity.add(Box.createHorizontalStrut(20));
        detailCity.add(city);
        this.add(detailCity);

        // NPA
        JPanel detailNPA = new JPanel();
        JLabel npaLabel = new JLabel("NPA");
        JLabel npa = new JLabel("1950");
        detailNPA.add(npaLabel);
        detailNPA.add(Box.createHorizontalStrut(20));
        detailNPA.add(npa);
        this.add(detailNPA);

        // Téléphone
        JPanel detailPhone = new JPanel();
        JLabel PhoneLabel = new JLabel("Numéro de Téléphone");
        JLabel phone = new JLabel("079 123 45 67");
        detailPhone.add(PhoneLabel);
        detailPhone.add(Box.createHorizontalStrut(20));
        detailPhone.add(phone);
        this.add(detailPhone);

        // Boutin de modification
        JButton modify = new JButton("Modification");
        this.add(modify);
    }
}
