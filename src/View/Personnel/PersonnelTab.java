package View.Personnel;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Andre on 10.03.2017.
 */
public class PersonnelTab extends JPanel {

    public PersonnelTab(){

        this.setLayout(new BorderLayout());

        // Création de notre boutton d'ajout de personnel
        JButton addPersonnelButton = new JButton("Ajout de personnel");
        JPanel addPersonnel = new JPanel();
        addPersonnel.add(addPersonnelButton);
        this.add(addPersonnel, BorderLayout.NORTH);


        PersonnelInfo personnel = new PersonnelInfo();
        this.add(personnel, BorderLayout.WEST);


    }
}
