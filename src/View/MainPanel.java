package View;

import View.Animal.AnimalTab;
import View.Personnel.PersonnelTab;
import View.Show.ShowTab;
import View.Stock.StockTab;

import javax.swing.*;

/**
 * Created by Andre on 10.03.2017.
 */
public class MainPanel extends JTabbedPane {

    public MainPanel() {
    this.add("Personnel", new PersonnelTab());
    this.add("Animal", new AnimalTab());
    this.add("Spectacle", new ShowTab());
    this.add("Stock", new StockTab());
    }

}
