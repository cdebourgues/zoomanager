package View.Animal;

import javax.swing.*;

/**
 * Created by Andre on 10.03.2017.
 */
public class AnimalTab extends JPanel {

    public AnimalTab(){

        // Liste déroulante avec les prénom et type d'animaux
        JComboBox boxChoiceAnimal = new JComboBox();
        JPanel choiceAnimal = new JPanel ();
        // Requête pour récupérer les nom des animaux
        boxChoiceAnimal.addItem("Elephant Bernard");
        boxChoiceAnimal.addItem("T-rex Alphonse");
        choiceAnimal.add(boxChoiceAnimal);
        this.add(choiceAnimal);


    }
}
