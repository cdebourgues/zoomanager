import View.MainPanel;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Andre on 10.03.2017.
 */
public class Control {
    public static void main(String[] args) {
        JFrame f = new JFrame("Zoo Manager");
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.getContentPane().add(new MainPanel(), BorderLayout.CENTER);
        f.setSize(800, 500);
        f.setVisible(true);
    }
}
